from fastapi import FastAPI, UploadFile
from pydantic import BaseModel
import numpy as np
from PIL import Image
from pathlib import Path
from tempfile import NamedTemporaryFile
import shutil

app = FastAPI()

class Colors(BaseModel):
    name : str
    RGB : list

colors_list = {}

@app.post('/api/v1/color/add')
def add_color(color: Colors):
    colors_list[color.name] = color.RGB
    return {color.name: color.RGB}


@app.post('/api/v1/image/detect')
def dominant_color(file: UploadFile):
    arr = open_file(file)
    color = []
    for row in arr:
        for temp_r, temp_g, temp_b, temp in row:
            color.append([temp_r, temp_g, temp_b])

    numbers_of_colors = {}

    for key in colors_list.keys():
        numbers_of_colors[key] = 0

    for key, value in colors_list.items():
        for c in color:
            if value == c:
                numbers_of_colors[key] += 1

    sort_orders = list({k: v for k, v in sorted(numbers_of_colors.items(), key=lambda item: item[1])})
    return {sort_orders[-1]: colors_list[sort_orders[-1]]}


def open_file(file):
    try:
        suffix = Path(file.filename).suffix
        with NamedTemporaryFile(delete=False, suffix=suffix) as tmp:
            shutil.copyfileobj(file.file, tmp)
            tmp_path = Path(tmp.name)
    finally:
        file.file.close()

    image = Image.open(tmp_path)
    arr = np.array(image)
    return arr